import pyautogui as pg
import time

# Creade New Desktop
pg.hotkey('ctrl', 'win', 'd')
time.sleep(2)

# Open Win Run
pg.hotkey('win', 'r')
time.sleep(2)

# Open Notepad
pg.write('notepad', interval=0.25)
pg.press('enter')
time.sleep(2)

# Write Message
pg.write('Hello world!', interval=0.25)
