# Set location for main path
$localPath = "your\path\toFolder" 
# Set venv Name
$venvName = "myvenv" 

# Switch to main path
cd -Path $localPath
# Run venv 
& ".\$venvName\Scripts\activate"
# Run python script
python main.py



