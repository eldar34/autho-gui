# INSTALLATION
Create venv

```
python -m venv myvenv
```

Run venv for Windows
```
myvenv\Scripts\activate
```

Run venv for Linux
```
source myvenv/bin/activate
```

Update pip
```
python -m pip install --upgrade pip
```

Install packages
```
pip install -r requirements.txt
```

# Script Settings
Set configuration in runner.ps1
```
$localPath = "your\path\toFolder" 
$venvName = "myvenv" 
...
```

# Usage from PowerShell
Run script from PowerShell
```
.\runner.ps1
```

# Usage from Windows Task Scheduler
Open Windows Run
```
win+rR
```

Open Windows Task Scheduler
```
taskschd.msc
```

Create new Task and set configuration
```
program powershell.exe
argument -Command "your\path\toFolder\runner.ps1"
```

